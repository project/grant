<?php

/**
 * @file
 * Views hooks for the Grant module.
 */

/**
 * Implements hook_views_data_alter().
 */
function grant_views_data_alter(array &$data) {

  $data['grant']['entity_data']['field'] = [
    'title' => t('Grant entity data'),
    'help' => t('Computed field to use entity data.'),
    'id' => 'grant_entity_data',
  ];

  $args = [
    '@label' => 'user account',
    '@entity' => 'grant',
  ];
  $account_fields = ['user', 'assignee'];

  foreach ($account_fields as $field_name) {
    $data['grant']['table']['join']['users_field_data'] = [
      'field' => $field_name,
      'left_field' => 'uuid',
      'table' => 'grant',
    ];

    $args['@field_name'] = 'grant ' . $field_name . ' field';
    // Provide a relationship for the entity type with the entity reference
    // field.
    $data['grant'][$field_name]['relationship'] = [
      'title' => t('@label referenced from @field_name', $args),
      'label' => t('@field_name: @label', $args),
      'group' => 'grant',
      'help' => t('Appears in each grant'),
      // @see \Drupal\views\Plugin\views\relationship\Standard
      'id' => 'grant_standard_uuid',
      'base' => 'users_field_data',
      'entity base table' => 'users',
      'entity uuid field' => 'uuid',
      'entity type' => 'user',
      'base field' => 'uid',
      'relationship field' => $field_name,
    ];

    // Provide a reverse relationship for the entity type that is referenced by
    // the field.
    $pseudo_field_name = 'reverse__grant__' . $field_name;
    $data['users_field_data'][$pseudo_field_name]['relationship'] = [
      'title' => t('@entity using @field_name', $args),
      'label' => t('Grant @field_name', ['@field_name' => $field_name]),
      'group' => 'user',
      'help' => t('Relate each @entity with a @field_name set to the @label.', $args),
      'id' => 'grant_reverse_uuid',
      'base' => 'grant',
      'entity base table' => 'grant',
      'entity_type' => 'grant',
      'base field' => 'uuid',
      'target base' => 'users_field_data',
      'target entity base table' => 'users',
      'target entity uuid field' => 'uuid',
      'target entity base field' => 'uid',
      'field_name' => $field_name,
      'field table' => 'grant',
      'field field' => $field_name,
    ];
  }

}
