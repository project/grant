<?php

namespace Drupal\grant_cm;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation\StateTransitionValidation;
use Drupal\content_moderation\StateTransitionValidationInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\grant\GrantMain;
use Drupal\user\Entity\User;
use Drupal\workflows\StateInterface;
use Drupal\workflows\Transition;
use Drupal\workflows\WorkflowInterface;

/**
 * Validates whether a certain state transition is allowed.
 */
class GrantStateTransitionValidation extends StateTransitionValidation implements StateTransitionValidationInterface {
  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * Stores the possible state transitions.
   *
   * @var array
   */
  protected $possibleTransitions = [];

  /**
   * The grant main service.
   *
   * @var \Drupal\grant\GrantMain
   */
  protected $grantMain;

  /**
   * Constructs a new StateTransitionValidation.
   *
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_info
   *   The moderation information service.
   * @param \Drupal\grant\GrantMain $grant_main
   *   The grant main service.
   */
  public function __construct(ModerationInformationInterface $moderation_info, GrantMain $grant_main) {
    $this->moderationInfo = $moderation_info;
    $this->grantMain = $grant_main;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidTransitions(ContentEntityInterface $entity, AccountInterface $user) {
    $workflow = $this->moderationInfo->getWorkflowForEntity($entity);
    $current_state = $entity->moderation_state->value ? $workflow->getTypePlugin()->getState($entity->moderation_state->value) : $workflow->getTypePlugin()->getInitialState($entity);
    $valid_transitions = [];

    $valid_transitions = array_filter($current_state->getTransitions(), function (Transition $transition) use ($workflow, $user, $entity) {
      $check_perm = 'use ' . $workflow->id() . ' transition ' . $transition->id();
      if ($user->hasPermission($check_perm)) {
        return TRUE;
      }

      $u3id = User::load($user->id())->uuid();
      if ($this->grantMain->userAssignedGrantHasPermission($u3id, $check_perm, $entity->getEntityTypeId(), $entity->uuid())) {
        return TRUE;
      }
    });

    return array_merge($valid_transitions, parent::getValidTransitions($entity, $user));
  }

  /**
   * {@inheritdoc}
   */
  public function isTransitionValid(WorkflowInterface $workflow, StateInterface $original_state, StateInterface $new_state, AccountInterface $user, ContentEntityInterface $entity = NULL) {
    $transition = $workflow->getTypePlugin()->getTransitionFromStateToState($original_state->id(), $new_state->id());

    $check_perm = 'use ' . $workflow->id() . ' transition ' . $transition->id();
    if ($user->hasPermission($check_perm)) {
      return TRUE;
    }
    $u3id = User::load($user->id())->uuid();
    if ($this->grantMain->userAssignedGrantHasPermission($u3id, $check_perm, $entity->getEntityTypeId(), $entity->uuid())) {
      return TRUE;
    }
    return parent::isTransitionValid($workflow, $original_state, $new_state, $user, $entity);
  }

}
