## INTRODUCTION
Grant allows to manage access to entities with a simple logic which can be used
standalone, and a more complex logic based on the grant entity and mail
addresses with assigning roles in "grant context".

Beside the possibility of an own grant access check which can be added to
custom or contrib routes, the most important part is done in
"hook_entity_access()".

With grant, you can realize several customized access logic and some simple
group and micro team logic. With grant_base alone you have the possibility to
create an entity access logic based on references to other entities, including
users and roles. And with the module you have "extended possibilities" to
realize a more complex logic and more privacy.

## Current state of development: It's still a dev version
It's current only recommended for developers.
__Important Issue__: [#3322149]

## EXTENDED POSSIBILITIES
The complex logic allows for a type of self-organized groups where groups are
only built via entity references. E.g. there are the node types "customer" and
another node type "wiki" which is connected via an entity reference field
"field_customer". Then there are the two core roles "customer_admin" and
"customer_staff". The customer admin should have the following permission:
"grant-assign invite".

When used with the grant assignment logic, these roles should not be globally
attached to the users.

Instead, you may "invite" a personal mail e.g.
`name1@example.com` with the role "customer_admin" to the customer node
"Customer X". This invitation is stored in a grant entity and will be shown
to a user who successfully registered and validated the mail address
`name1@example.com`. Now these users may also invite other associates by their
role "customer_staff" and tell them to register on this site.
With the multiple_email module, more than one mail address can be used.

So the grant module allows for organized grant role assignments without
accessing user data and even knowing if a person already has an account or not.
The upcoming optional grant mail option will reflect this privacy. Because grant
entities base on entity_reference_uuids (required), every entity can be
addressed by its (unique) UUID.

When read and write access to wiki nodes are configured as permissions with the
role "customer_staff", there is now a wiki group. Beside the options based on
mail addresses, this simple group can also be realized with the
grant_base.module, but greater and more complex structures can be created with
parallel groups, e.g. a support team entity connected to "Customer X" with extra
support roles which have different permissions etc.
The current development of this module is based on a complex structure of many
custom entities for internal ressources and an external organisation structure,
e.g. customer units, and vendor data. Because all data needs to be connected
via references, why not also use them for access management?

## TECHNICAL LIMITATIONS
Because this module is developed with a focus on custom entities to create
managed access content directly related to each other, systems where this
is combined with other entity strategies might introduce problems.

**As long we only look at hook_entity_access(), the usage of views or custom
database operations eventually lead to an information disclosure problem.**
https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_entity_access
@todo Add more restriction strategies.

Nodes (content) of the core can be integrated in a basic way, but because of
the special core access strategy be aware of two things:
1. Each role incl. anonymous needs the permission "View published content".
Otherwise the hook_entity_access won't allow access.
2. A user with node permission "Bypass content access control" can not be
blocked by `AccessResult::forbidden()` in hook_entity_access().

## REQUIREMENTS
[entity_reference_uuid](https://www.drupal.org/project/entity_reference_uuid)

## Suggested modules
[multiple_email](https://www.drupal.org/project/multiple_email)

## INSTALLATION
Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
There currently is no special browser UI. The access logic can only be executed
via settings.php (see example below). All entity types, bundles, and fields need
to be configured via machine readable names. The grnat module provides some
views plugins to build Grant lists and content lists based on roles and
permissions in grant context. So views can be used to organize content.

Especially when using the simple logic with entity reference fields, be careful
of who has access to a specific field and which values can be set. **Grant
currently provides no help for this**. So please use hook_form_alter() or a
helper module like this:
[field_permissions](https://www.drupal.org/project/field_permissions)

Additionally you probably want to add permissions to admin roles because they
are also checked on current user global permission.
And of course you usually need to create special grant roles (as described
above) and maybe add more fields if needed to build or enhance your content
structures.

## INTEGRATION
Core 'content_moderation' can be integrated with main module via submodule
'grant_cm' to check permissions in gran assigned roles. Otherwise it's blocking
update operation on entity_access. Or you can use permissions via global
assigned roles which is currently the only way to integrate base
'access_ops_direct'.

### Settings Options:
All configuration for entities follows this form:
`$settings['grant']['entities'][ENTITY_TYPE][BUNDLE]`
Replace upper case words with the machine readable names e.g. `node` and
`article`. The "access_*" configuration can be used with the grant_base.module.
It will be checked before the assign logic of the grant.module fires.

The "allowed_if" flag controls the behavior of the hook_entity_access.
This means, if true it will return `AccessResult::allowedIf`.
This is an additional allowance check for integration with other modules.
Otherwise, grant_base.module will return `AccessResult::forbidden().`
If not defined, it is set to "FALSE" by default.

`access_ops_direct` is used to simply allow access via another
entity with entity_reference or entity_reference_uuid fields.
If the 'target_type' (target entity type) of the field is a role or a user, the
configured `ops` (opperations) are allowed without any further check, even
`update` or `delete`. If connected to other entities, the grant_base.module
will just perform an access check on the referenced entity with the same
requested 'operation'.

The "assign_" configuration is only used when the grant.module is enabled.

### Example config in settings.php
```php
$settings['grant']['entities']['node']['article'] = [
  'access_allowed_if' => FALSE,

  'access_ops_direct' => [
    'field_article' => ['ops' => ['view']],
    'field_access_parent_author' => ['ops' => ['view', 'update']],
    'field_roleuuid' => ['ops' => ['view']],
    'field_useruuid' => ['ops' => ['view']],
  ],

  // Role and permission checks are at first be checked globally and also on
  // grant assigned roles when 'assign_access' is enabled. On entity_access
  // situation it's possible to combine role and permission checks.
  'entity_access_role' => [
    'view' => ['group_moderator', 'group_guest'],
    'update' => ['group_moderator'],
  ],
  'entity_access_perm' => [
    'view' => ['access content'],
    'update' => ['administer users'],
  ],
  // Add check on permission "grant-base: entity access {operation}"
  // Default is "TRUE" because it was checked per default in earlier code.
  // Only enable if you need because every check has impact on performance:
  'entity_access_perm_base' => FALSE,

  // 'assign_access' enables the grant assign roles/permission logic on
  // entity access_check in grant_base.
  'assign_access' => TRUE,
  // 'assign_parents' follows the assignment/next parent on referenced entity.
  'assign_parents' => ['field_reference_to_other entity'],
  // 'assign_control' defines which role incl grant assigned role can invite
  // other users (based on mail address) with which role.
  'assign_control' => [
    'group_admin' => ['group_moderator', 'group_guest'],
    'second_admin_role' => ['group_guest'],
  ],
  // 'assign_invite_link' activate extra field for mail/role assign invitation.
  'assign_invite_link' => TRUE,
  // If 'assign_auto' is TRUE, a user will automatically be connected.
  'assign_auto' => FALSE,
];
```

## Old (obsolete) D7 concept and new strategy on D9+
The old idea and first D7 code was build on an field type. In this concept there
was a need on multiple field definitions to manage multiple permissions. The new
concept is based roles and permissions of modules to integrate grant module.
So at the beginning this module needs custom or additional contrib code to work.
When this is working fine we are open for contributions to get some configurable
stuff to interact with core entities like nodes etc. Also the old concept of
"external permissions" apart from the Drupal system to control access to other
resources e.g. other web applications (including drupal) for SSO (single sign
on) functionality etc. will be easy to integrate with custom code.
