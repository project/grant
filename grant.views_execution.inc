<?php

/**
 * @file
 * Provide views runtime hooks for the Grant module.
 */

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 *
 * Alter query as workaround on core bug for default argument.
 *
 * @see GrantCurrentUserEmails.php
 */
function grant_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  $arg = $view->argument;
  if (isset($arg['email'])
      && $arg['email']->options['default_argument_type'] == "grant_current_user_emails"
      && \Drupal::moduleHandler()->moduleExists('multiple_email')) {

    $field = $arg['email']->table . ".email";
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        if ($condition['field'] == $field) {
          $condition = [
            'field' => $field,
            'value' => \Drupal::service('grant.main')->getUserEmails(),
            'operator' => 'in',
          ];
        }
      }
    }
  }

}
