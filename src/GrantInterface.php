<?php

namespace Drupal\grant;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a grant entity type.
 */
interface GrantInterface extends ContentEntityInterface, EntityChangedInterface {

}
