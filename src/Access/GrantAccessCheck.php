<?php

namespace Drupal\grant\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\grant\GrantMain;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on permissions and roles via grant.
 */
class GrantAccessCheck implements AccessInterface {

  /**
   * Grant main service.
   *
   * @var \Drupal\grant\GrantMain
   */
  protected $grantMain;

  /**
   * The GrantAccessCheck constructor.
   *
   * @param \Drupal\grant\GrantMain $grantMain
   *   The grant main service.
   */
  public function __construct(GrantMain $grantMain) {
    $this->grantMain = $grantMain;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('grant.main')
    );
  }

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route) {
    $gra_config_raw = $route->getRequirement('_grant_access_check');
    if ($gra_config_raw === NULL) {
      return AccessResult::neutral();
    }
    $access = FALSE;

    $options = ['context' => 'GrantAccessCheck'];
    // Grant Access Check Config e.g.:
    // "_grant_access_check: 'perm|access grant invite|type:3,uuid:4'".
    $gra_config = explode("|", $gra_config_raw);
    if (isset($gra_config[1])) {
      $gra_config_arg_list = explode(',', trim($gra_config[1]));
      foreach ($gra_config_arg_list as $gra_config_arg_raw) {
        $key_value = explode(':', trim($gra_config_arg_raw));
        $key = trim($key_value[0]);
        $value = 0;
        if (isset($key_value[1])) {
          if ($key == 'type_name') {
            $options['e_type'] = $key_value[1];
          }
          else {
            $value_raw = trim($key_value[1]);
            $value = (int) $value_raw;
            if ($key == 'type') {
              $options['e_type_arg'] = $value;
            }
            if ($key == 'uuid') {
              $options['e_id_type'] = 'uuid';
              $options['e_id_arg'] = $value;
            }
            else {
              $options['e_id_type'] = 'serial';
              $options['e_id_arg'] = $value;
            }
          }
        }
      }
    }

    $check_raw = explode(':', trim($gra_config[0]));
    $check_value = '_none_';
    if (isset($check_raw[1])) {
      $check_type = $check_raw[0];
      $check_value = trim($check_raw[1]);
    }
    else {
      // For bachward compatibility set default on 'perm'.
      $check_type = 'perm';
      $check_value = trim($check_raw[0]);
    }
    switch ($check_type) {
      case 'perm':
        $options['grant_perm'] = $check_value;
        break;

      case 'role':
        $options['grant_role'] = $check_value;
        break;
    }
    $access = $this->grantMain->userAssignedGrantHasAccessPathCurrent($options, NULL);

    return AccessResult::allowedIf($access);
  }

}
