<?php

namespace Drupal\grant\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\grant\GrantMain;
use Drupal\user\PermissionHandlerInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides grant context permission-based access control.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "grant_perm",
 *   title = @Translation("Grant Context Permission"),
 *   help = @Translation("Access will be allowed to users with the specified permission via globally or grant assigned roles.")
 * )
 */
class GrantPermission extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The grant main service.
   *
   * @var \Drupal\grant\GrantMain
   */
  protected $grantMain;

  /**
   * Constructs a Permission object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\grant\GrantMain $grant_main
   *   The grant main service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler, GrantMain $grant_main) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
    $this->grantMain = $grant_main;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('module_handler'),
      $container->get('grant.main'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $access = $this->grantMain->userAssignedGrantHasAccessPathCurrent($options, $account);
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_grant_access_check', $this->grantMain->createGrantAccessCheckString($this->options));
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->grantMain->createGrantAccessCheckString($this->options);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['grant_perm'] = ['default' => 'administer grant'];
    $options['e_type'] = ['default' => ''];
    $options['e_type_arg'] = ['default' => 1];
    $options['e_id_type'] = ['default' => 'uuid'];
    $options['e_id_arg'] = ['default' => 2];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // Get list of permissions:
    $perms = [];
    $permissions = $this->permissionHandler->getPermissions();
    foreach ($permissions as $perm => $perm_item) {
      $provider = $perm_item['provider'];
      $display_name = $this->moduleHandler->getName($provider);
      $perms[$display_name][$perm] = strip_tags($perm_item['title']);
    }
    $path_options = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6];
    $e_value_type_options = ['uuid' => 'UUID', 'serial' => 'serial ID'];
    $form['grant_perm'] = [
      '#type' => 'select',
      '#options' => $perms,
      '#title' => $this->t('Permission in Grant Context'),
      '#default_value' => $this->options['grant_perm'],
      '#description' => $this->t('Only users with the selected permission inside grant context will be able to access this display.'),
    ];
    $form['e_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity type: machine name.'),
      '#default_value' => $this->options['grant_type'],
      '#description' => $this->t('Use this string e.g. "node" for a "hard" coded config or the path section below.'),
    ];
    $form['e_type_arg'] = [
      '#type' => 'select',
      '#options' => $path_options,
      '#title' => $this->t('Entity type: path segment'),
      '#default_value' => $this->options['grant_type_path'],
      '#description' => $this->t('Will be ignored if machine name above is not empty.'),
    ];
    $form['e_id_type'] = [
      '#type' => 'select',
      '#options' => $e_value_type_options,
      '#title' => $this->t('Entity ID: type'),
      '#default_value' => $this->options['e_id_type'],
      '#description' => $this->t('Select if entity id to check is UUID or serial ID like "nid".'),
      '#disabled' => FALSE,
    ];
    $form['e_id_arg'] = [
      '#type' => 'select',
      '#options' => $path_options,
      '#title' => $this->t('Entity ID: path segment'),
      '#default_value' => $this->options['e_id_arg'],
      '#description' => $this->t('In which path segment contains the uuid or serial id of the entoty to check.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @todo add grant cache contexts
    // return ['user.permissions'];
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // @todo add grant cache tags
    return [];
  }

}
