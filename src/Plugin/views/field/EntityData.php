<?php

declare(strict_types=1);

namespace Drupal\grant\Plugin\views\field;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\grant\GrantInterface;
use Drupal\grant\GrantMain;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Grant Entity Link field handler.
 *
 * @ViewsField("grant_entity_data")
 */
final class EntityData extends FieldPluginBase {

  /**
   * Constructs a new EntityLink instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly GrantMain $grantMain,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('grant.main'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['type'] = ['default' => 'label'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $select = [
      'bundle' => $this->t('Entity bundle'),
      'id' => $this->t('Entity ID (serial)'),
      'label' => $this->t('Entity label'),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#key_type' => 'associative',
      '#options' => $select,
      '#title' => $this->t('Type'),
      '#default_value' => $this->options['type'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // For non-existent columns (i.e. computed fields) this method must be
    // empty.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): string|MarkupInterface {
    $type = $this->options['type'];
    $value = ['none'];
    $entity = $values->_entity;
    if ($entity instanceof GrantInterface) {
      $grant = $entity;
    }
    else {
      $grant = NULL;
      $re = $values->_relationship_entities ?? [];
      if (isset($re['reverse__grant__assignee'])) {
        $grant = $re['reverse__grant__assignee'];
      }
      if (isset($re['reverse__grant__user'])) {
        $grant = $re['reverse__grant__user'];
      }
    }
    if ($grant instanceof GrantInterface) {
      $e_type = $grant->get('entity_type')->value;
      $e_uuid = $grant->get('entity_uuid')->value;
      $entity = $this->grantMain->entityLoadUuid($e_type, $e_uuid);
      switch ($type) {
        case 'bundle':
          $value = $entity->bundle();
          break;

        case 'id':
          $value = $entity->id();
          break;

        case 'label':
          $value = $entity->label() ?? '[' . $this->t('no label') . ']';
          break;

        default:
          $value = '';
          break;
      }
    }
    return $value;
  }

}
