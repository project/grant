<?php

namespace Drupal\grant\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GrantCurrentUserUuid argument default plugin.
 *
 * @ViewsArgumentDefault(
 *   id = "grant_current_user_uuid",
 *   title = @Translation("User UUID from logged in user (grant module)")
 * )
 */
class GrantCurrentUserUuid extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * Constructs a new GrantCurrentUserUuid instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly AccountInterface $account,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $uid = $this->account->id();
    $current_user = $this->entityTypeManager->getStorage('user')->load($uid);
    return $current_user->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

}
