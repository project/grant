<?php

namespace Drupal\grant\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\grant\GrantInterface;
use Drupal\user\Entity\User;

/**
 * Defines the grant entity class.
 *
 * @ContentEntityType(
 *   id = "grant",
 *   label = @Translation("Grant"),
 *   label_collection = @Translation("Grants"),
 *   label_singular = @Translation("grant"),
 *   label_plural = @Translation("grants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count grants",
 *     plural = "@count grants",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\grant\GrantListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\grant\Form\GrantForm",
 *       "edit" = "Drupal\grant\Form\GrantForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\grant\Routing\GrantHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "grant",
 *   admin_permission = "administer grant",
 *   entity_keys = {
 *     "id" = "uuid",
 *     "label" = "email",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/grant",
 *     "add-form" = "/grant/add",
 *     "canonical" = "/grant/{grant}",
 *     "edit-form" = "/grant/{grant}",
 *     "delete-form" = "/grant/{grant}/delete",
 *   },
 *   field_ui_base_route = "entity.grant.settings",
 * )
 */
class Grant extends ContentEntityBase implements GrantInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);

    $user = User::load(\Drupal::currentUser()->id());
    $values += [
      'user' => $user->uuid(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['note'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Invitation Note'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created/Invited'))
      ->setDescription(t('The time that the grant invitation was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the grant was last edited.'));

    $fields['user'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Grant user'))
      ->setSetting('max_length', 128)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['assignee'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Assignee'))
      ->setSetting('max_length', 128)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['ignore'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Assignee ignore invitation'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Ignored')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['role'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Grant role'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user_role')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 64)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity UUID'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Grant email'))
      ->setDescription(t('The mail to grant.'))
      ->setSettings([
        'max_length' => 254,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['assigned'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Assigned'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDescription(t('The time that the grant invitation was accepted.'));

    $fields['unassigned'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Unassigned'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDescription(t('The time that the grant invitation was unassigned.'));

    $fields['expiration'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Expiration'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
