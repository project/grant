<?php

namespace Drupal\grant\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\grant\GrantMain;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Grant form.
 */
class GrantInviteForm extends FormBase {

  /**
   * Active user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  public $account;

  /**
   * The alias manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Grant main service.
   *
   * @var \Drupal\grant\GrantMain
   */
  protected $grantMain;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The active user account.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Path alias manager.
   * @param \Drupal\grant\GrantMain $grant_main
   *   The grant main service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    AccountProxyInterface $account,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    GrantMain $grant_main,
  ) {
    $this->account = $account;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->grantMain = $grant_main;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('grant.main'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'grant_grant_invite';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $type = '', $uuid = '') {
    $parameter = [];
    $e_type = Html::escape($type);
    $e_uuid = Html::escape($uuid);
    $entity = $this->grantMain->entityLoadUuid($e_type, $e_uuid);
    if (!$entity instanceof ContentEntityInterface) {
      // @todo Throw Error.
      return;
    }
    $e_bundle = $entity->bundle();

    $grant_configs = $this->grantMain->getGrantConfigs() ?? [];
    $config = $grant_configs['entities'][$e_type][$e_bundle] ?? [];

    $form['label'] = [
      '#markup' => '<h2>' . $entity->label() . '</h2>',
    ];

    $parameter['entity_type'] = $e_type;
    $parameter['entity_uuid'] = $e_uuid;
    // $parameter['user'] = $this->account->uuid();
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $rids_all = array_keys($roles);
    $roles_select = [];

    $account_roles = $this->account->getRoles();
    $uag_rids = $this->grantMain->getUserAssignedGrantRoles('current', $type, $uuid);
    $current_user_all_rids = array_merge($account_roles, $uag_rids);
    $assign_control = $config['assign_control'] ?? [];
    foreach ($assign_control as $control_role => $assign_roles) {
      if (in_array($control_role, $current_user_all_rids)) {
        foreach ($assign_roles as $assign_role) {
          if (in_array($assign_role, $rids_all)) {
            $roles_select[$assign_role] = $roles[$assign_role]->label();
          }
        }
      }
    }
    if ($roles_select == []) {
      $message = $this->t('No roles configured as grant config or as user_role.');
      $this->messenger()->addWarning($message);
    }
    else {
      $form['role'] = [
        '#title' => 'Grant Role',
        '#type' => 'select',
        '#key_type' => 'associative',
        '#options' => $roles_select,
        '#description' => $this->t('Which role you want to assign.'),
        '#weight' => 1,
      ];

      $form['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Grant email address'),
        '#description' => $this->t('Email address of a person you want to invite.'),
        '#weight' => 0,
      ];

      $form['note'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Note'),
        '#description' => $this->t('Optional adminitration note.'),
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Invite user'),
      ];
    }
    $form_state->setStorage($parameter);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Do validation
    // $parameter = $form_state->getStorage();
    // if (mb_strlen($form_state->getValue('message')) < 10) {
    // $form_state->setErrorByName('message',
    // $this->t('Message should be at least 10 characters.'));
    // }.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameter = $form_state->getStorage();
    $values = $form_state->getValues();
    $values['entity_type'] = $parameter['entity_type'];
    $values['entity_uuid'] = $parameter['entity_uuid'];

    $this->grantMain->addGrant($values);

    // @todo Check new grant entity for success and modify message auto create.
    $this->messenger()->addStatus($this->t('The grant invitation has been saved.'));
    // @todo Make redirect configurable.
    // $form_state->setRedirect('<front>');
  }

}
