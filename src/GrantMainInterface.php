<?php

declare(strict_types=1);

namespace Drupal\grant;

/**
 * Provide main class for grant module.
 */
interface GrantMainInterface {

  /**
   * Retrieve grant config of all entity types.
   *
   * Set defaults and get config from settings.php.
   */
  public function getGrantConfigs();

  /**
   * Load entiy by uuid.
   */
  public function entityLoadUuid($e_type = '', $e_uuid = '');

  /**
   * Add grant entity.
   */
  public function addGrant($values = []);

  /**
   * Manage cache deletion for assignee user.
   */
  public function grantChangeCacheClear($grant_entity);

  /**
   * Create the config string for route requirement "_grant_access_check".
   */
  public function createGrantAccessCheckString($options = []);

  /**
   * Check if user has access to path via assigned grants.
   */
  public function userAssignedGrantHasAccessPathCurrent($options = [], $account = NULL);

  /**
   * Retrieve assigned grant roles of a user.
   */
  public function getUserAssignedGrantRoles($u3id = 'current', $e_type = '', $e_uuid = '');

  /**
   * Retrieve user assigned grants.
   */
  public function getUserAssignedGrants($uuid = "current");

  /**
   * Check if user has permission via assigned grant roles.
   */
  public function userAssignedGrantHasPermission($u3id = 'current', $perm_raw = '', $e_type = '', $e_uuid = '', $role_limit = [], $uag_roles = []);

}
